# eslint-config-ota

Shareable eslint config for OTA Node projects using recommended eslint/prettier configs.

### Getting Started
You can install this package directly from gitlab
`npm i --save-dev @onetapaway/eslint-config-ota`

Next the peer dependencies will need to be installed manually
`npm i --save-dev eslint eslint-config-prettier eslint-plugin-prettier prettier eslint-plugin-jest`

Inside your node project's package.json file add the following eslintConfig:
```json
  "eslintConfig": {
    "extends": "eslint-config-ota"
  }
```
OTA configuration rules can be overridden on a per project basis by adding a rules object to the eslintConfig object in the project's package.json file.
```json
  "eslintConfig": {
    "extends": "eslint-config-ota",
    "rules": {
      "no-unused-vars": "warn"
    }
  }
```

The above will set eslint's `no-unused-vars` rule to the level of `warning` instead of it's default of `error`

### Usage
Run eslint and prettier in a single command:
`npx eslint "*.js"`

To automatically fix problems:
`npx eslint "*.js" --fix`

[See complete list of options available](https://eslint.org/docs/user-guide/command-line-interface#options)

For ease of running consider adding a lint command to your `package.json`:
```json
"scripts": {
  "lint": "eslint \"**/*.js\""
}
```

### Setting up precommit hooks (optional)
We can setup precommit hooks in our projects to automatically run and fix any violations before a file is committed to git.

Install husky and lint-staged to your project
`npm i --save-dev husky lint-staged`

Add the following configuration to your project's package.json
```json
  "husky": {
    "hooks": {
      "pre-commit": "lint-staged"
    }
  },
  "lint-staged": {
    "*.js": [
      "npx eslint --fix",
      "git add"
    ]
  }
```

Now upon running `git commit` a pre-commit hook will run the eslint command for you. Any violations that aren't automatically fixed will need to be fixed by hand before committing your code.