module.exports = {
  plugins: ["jest"],
  extends: ["eslint:recommended", "plugin:prettier/recommended", "plugin:jest/recommended"],
  env: {
    browser: true,
    es6: true,
    node: true,
    "jest/globals": true
  },
  parserOptions: {
    ecmaVersion: 2022,
    sourceType: "module",
    ecmaFeatures: {
      defaultParams: true,
      spread: true
    }
  },
  rules: {
    // override general ESLint rules
    //i.e."no-unused-vars": "warn",

    // override recommended prettier rules for https://github.com/prettier/eslint-plugin-prettier
    "prettier/prettier": [
      "error",
      {
        // keep this in sync with .prettier.js (or .prettierrc)
        printWidth: 100,
        semi: true,
        singleQuote: true,
        tabWidth: 2,
        useTabs: true,
        trailingComma: "none"
      }
    ]
  }
};
